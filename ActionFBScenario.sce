# Eventcodes:
# 
# 1  - correct answer, contingent feedback
# 2  - wrong answer,   contingent feedback
# 11 - correct answer, noncontingent feedback
# 12 - wrong answer,   noncontingent feedback
#
# 3  - No reaction
#
# 101 - Buttonset 1 button 1
# 102 - Buttonset 1 button 2
# 201 - Buttonset 2 button 1
# 202 - Buttonset 2 button 2



scenario = "ActionFB";

response_matching = simple_matching;
active_buttons = 4;
write_codes = false;
pulse_width = 50;
default_font = "Arial";
button_codes = 101,102,201,202;

begin;

$proband = EXPARAM("Proband");

array {
	LOOP $i 10; $nr = '$i+1';
		trial{
			trial_type = first_response;
			trial_duration = forever;
			picture {bitmap {filename = "Instruktionen/Folie$nr.png"; }; x = 0; y = 0;};
			duration = response;
		};		
	ENDLOOP;
} Instructions;

picture { text {caption="+"; font="Segoe UI"; font_size=50; font_color=125,108,2;  }; x = 0; y = 0; } FixDunkel;
picture { text {caption="+"; font="Segoe UI"; font_size=50; font_color=255,233,127; }; x = 0; y = 0; } FixHell;

trial{
	trial_type = correct_response;
	stimulus_event{ 
		picture FixDunkel; 
		time = 0;
		code = "fix_dunkel";
		duration = next_picture;
	};
	stimulus_event{
		picture FixHell; 
		delta_time= 1000;
		code = "fix_hell";
		duration = 3500;
		target_button = 1,2;
	};
} Fix;

$stim=EXPARAM("Positiver Stimulus");


array{
	trial{ #Positiv
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = "FixHell";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {
				polygon_graphic {
					IF '$stim == 1'; # Dreieck
						sides = 3;
					ENDIF;
					IF '$stim == 2'; # Quadrat
						sides = 4;
						rotation  = 45;
					ENDIF;
					radius = 200;
					line_color = 255,255,255;
					line_width = 10;
					fill_color = 0,0,0;
				}; x = 0; y = 0;
			};
			time=EXPARAM ("Delay": 500);
			duration = 1000;
		};
	} ;

	trial{ #Negativ
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = "FixHell";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {
				polygon_graphic {
					IF '$stim == 2'; # Dreieck
						sides = 3;
					ENDIF;
					IF '$stim == 1'; # Quadrat
						sides = 4;
						rotation  = 45;
					ENDIF;
					radius = 200;
					line_color = 255,255,255;
					line_width = 10;
					fill_color = 0,0,0;
				}; x = 0; y = 0;
			};
			time=EXPARAM ("Delay": 500);
			duration = 1000;
		};
	} ;
	trial{ #lateClick
		stimulus_event{ 
			picture {text {caption=" "; font_color=255,233,127; font_size=32; }; x = 0; y = 0;};
			time = 0;
			code = "Black";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {text {caption="Bitte schneller reagieren."; font_color=255,233,127; font_size=32; }; x = 0; y = 0;};
			duration = 1000;
			time = 500;
		};
	} ;
} Feedback;


	trial{ #Pause
		stimulus_event{ 
			picture FixHell; 
			time = 0;
			code = "FixHell";
			duration = next_picture;
		};
		stimulus_event{ 
			picture {text {caption="Kurze Pause. Weiter mit einem Tastendruck."; font_color=255,233,127; font_size=32; }; x = 0; y = 0;};
			duration = 1000;
			time = 500;
		} ;
	} Pause;

begin_pcl;

	int delay = parameter_manager.get_int("Delay");
	int pos_stim = parameter_manager.get_int("Positiver Stimulus");

	int target = random(1,2);	
	int Money = 0;
	
	array <string> stimname [2] = {"Dreieck", "Quadrat"};

	array <int> uebung [10];
	uebung.fill( 1, 7, 1, 0);   #pos
	uebung.fill( 8, 10, 2, 0);  #neg
	uebung.shuffle();

	array <int> results [50];
	results.fill( 1, 35, 1, 0);   #pos
	results.fill( 36, 50, 2, 0); #neg
	results.shuffle();

	string proband = parameter_manager.get_string("Proband");
	logfile.set_filename(proband + ".log");

	array <int> jitter [11] = {1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500};

	output_file behaviour = new output_file();
	behaviour.open(proband + "_behaviour.csv");
	behaviour.print_line("version;delay;trial;target;button;feedback;code;money;rt");
	
	output_file testbehaviour = new output_file();
	testbehaviour.open(proband + "_testbehaviour.csv");
	testbehaviour.print_line("version;delay;trial;target;button;feedback;code;rt");

	sub Test_trial (int j) begin
		int time = clock.time();	
		int jit = jitter[random(1,11)];
		Fix.get_stimulus_event(2).set_delta_time(jit);
		Fix.present();
		response_data data = response_manager.last_response_data();
		int button = data.button();
		int rt = clock.time()-time;
		int feedback = 0;
		int code = 0;
		
		if(rt > 3500 + jit) then 
			feedback = 3; code = 3; 
			Feedback[feedback].present();
		end;

		testbehaviour.print("Test");   testbehaviour.print(";");
		testbehaviour.print(delay);    testbehaviour.print(";");
		testbehaviour.print(j);        testbehaviour.print(";");
		testbehaviour.print(target);   testbehaviour.print(";");
		testbehaviour.print(button);   testbehaviour.print(";");
		testbehaviour.print(feedback); testbehaviour.print(";");
		testbehaviour.print(code);     testbehaviour.print(";");
		testbehaviour.print(rt);       testbehaviour.print_line(" ");
	end;
	
	sub Main_trial (int i) begin
		int code=0;
		int time = clock.time();	
		int jit = jitter[random(1,11)];
		Fix.get_stimulus_event(2).set_delta_time(jit);
		Fix.present();
		response_data data = response_manager.last_response_data();
		int button = data.button();
		int rt = clock.time()-time;
		int feedback = 0;
		if (button == target) then
			feedback = results[i];
			Money = Money + 20;
			if(feedback == pos_stim) then
				code = 1;
			else
				code = 11;
			end;
		else
			feedback = 3 - results[i]; #inverted
			Money = Money - 10;
			if(feedback != pos_stim) then
				code = 2;
			else
				code = 12;
			end;
		end;
		
		if(rt > 3500 + jit) then feedback = 3; code = 3; end;
		
		Feedback[feedback].get_stimulus_event(2).set_port_code(code);
		Feedback[feedback].get_stimulus_event(2).set_event_code(string(code));
		Feedback[feedback].present();
		
		behaviour.print(stimname[pos_stim]); behaviour.print(";");
		behaviour.print(delay);    behaviour.print(";");
		behaviour.print(i);        behaviour.print(";");
		behaviour.print(target);   behaviour.print(";");
		behaviour.print(button);   behaviour.print(";");
		behaviour.print(feedback); behaviour.print(";");
		behaviour.print(code);   	behaviour.print(";");
		behaviour.print(Money);   	behaviour.print(";");
		behaviour.print(rt);       behaviour.print_line(" ");
	end;

	Instructions[1].present();
	if(pos_stim == 1) then 
		Instructions[2].present();
	end;
	if(pos_stim == 2) then 
		Instructions[3].present();
	end;
	Instructions[4].present();

	loop int i=1 until i > uebung.count() begin
	#loop int i=1 until i > 0 begin
		int time = clock.time();	
		int jit = jitter[random(1,11)];
		Fix.get_stimulus_event(2).set_delta_time(jit);
		Fix.present();
		response_data data = response_manager.last_response_data();
		int button = data.button();
		int rt = clock.time()-time;
		int feedback = 0;
		if (button == target) then
			feedback = uebung[i];
		else
			feedback = 3 - uebung[i];
		end;
		if(rt > 3500 + jit) then feedback = 3; end;
		Feedback[feedback].present();
		i = i + 1;
	end;
	
	Instructions[10].present();

	#No Feedback
	loop int i=1 until i > uebung.count() begin
	#loop int i=1 until i > 0 begin
		int time = clock.time();	
		int jit = jitter[random(1,11)];
		Fix.get_stimulus_event(2).set_delta_time(jit);
		Fix.present();
		response_data data = response_manager.last_response_data();
		int button = data.button();
		int rt = clock.time()-time;
		int feedback = 0;
		if (button == target) then
			feedback = uebung[i];
		else
			feedback = 3 - uebung[i];
		end;
		if(rt > 3500 + jit) then 
			feedback = 3; 
			Feedback[feedback].present();
		end;
		#Feedback[feedback].present();
		i = i + 1;
	end;
	
	target = random(1,2);

	Instructions[5].present();
	loop int i=1 until i > results.count() begin
		Main_trial(i);
		i = i + 1;
	end;
	
	Instructions[6].present();
	loop int j=1 until j > 20 begin
		Test_trial(j);
		j = j + 1;
	end;

	Instructions[8].present();
	array<int> newTargets[2] = {3,4};
	target = random(3,4);
	Fix.get_stimulus_event(2).set_target_button(newTargets);
	loop int i=1 until i > results.count() begin
		Main_trial(i);
		i = i + 1;
	end;
	
	Instructions[6].present();
	loop int j=1 until j > 20 begin
		Test_trial(j);
		j = j + 1;
	end;

	Instructions[9].present();
	behaviour.close();